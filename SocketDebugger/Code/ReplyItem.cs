﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Xml;

namespace SocketClient
{
    public class ReplyGroupManager
    {
        public static ReplyGroupManager Instance
        {
            get
            {
                return _instance;
            }
        }private static ReplyGroupManager _instance = new ReplyGroupManager ();

        private ReplyGroupManager()
        {

        }
        public ReplyGroup this[string name]
        {
            get
            {
                foreach (var item in this.ReplyGroups)
                {
                    if (string.Compare(item.Name.Trim(), name.Trim(), true) == 0)
                    {
                        return item;
                    }
                }

                ReplyGroup newItem = new ReplyGroup(name);
                this.ReplyGroups.Add(newItem);
                return newItem;
            }
        }

        #region ReplyGroups
        /// <summary>
        /// 
        /// </summary>
        public ReplyGroupCollection ReplyGroups
        {
            get
            {
                if (_replyGroups == null)
                {
                    _replyGroups = new ReplyGroupCollection();
                }
                return _replyGroups;
            }
            set
            {
                _replyGroups = value;
            }
        } private ReplyGroupCollection _replyGroups;
        #endregion //ReplyGroups

    }
    public class ReplyGroupCollection : List<ReplyGroup>
    {
        public ReplyGroup GetFirstOrDefault()
        {
            if (this.Count > 0)
            {
                return this[0];
            }
            else
            {
                this.Add(new ReplyGroup("default"));
                return this[0];
            }
            
        }
    }
    public class ReplyGroup : IComparable<ReplyGroup>
    {
        public ReplyGroup(string name)
        {
            this.Name = name;
        }
        #region Name
        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get
            {
                if (_name == null)
                {
                    _name = string.Empty;
                }
                return _name;
            }
            set
            {
                _name = value;
            }
        } private string _name;
        #endregion //Name


        public int CompareTo(ReplyGroup other)
        {
            return this.Name.CompareTo(other.Name);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ReplyItem : IComparable<ReplyItem>
    {
        public string ReceivedPattern;
        public byte[] ReplyBytes;
        public string Name;
        public string Description;
        public bool Enabled;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public bool Match(string s)
        {
            if (Enabled)
            {
                Regex reg = new Regex(ReceivedPattern);
                return reg.IsMatch(s);
            }
            else
            {
                return false;
            }
        }

        public bool Match(byte[] bytes)
        {
            if (bytes == null ||
                bytes.Length == 0)
            {
                return false;
            }

            string s = (string)HexStringConverter.Default.ConvertToObject(bytes);
            return Match(s);
        }

        public ReplyItem()
        {

        }

        public ReplyItem(string name, string description, bool enabled, string receivedPattern, byte[] replyBytes)
        {
            this.Name = name;
            this.Description = description;
            this.Enabled = enabled;
            this.ReceivedPattern = receivedPattern;
            this.ReplyBytes = replyBytes;
        }

        #region ReplyGroup
        /// <summary>
        /// 
        /// </summary>
        public ReplyGroup ReplyGroup
        {
            get
            {
                if (_replyGroup == null)
                {
                    _replyGroup = ReplyGroupManager.Instance["Noname"];
                }
                return _replyGroup;
            }
            set
            {
                _replyGroup = value;
            }
        } private ReplyGroup _replyGroup;
        #endregion //ReplyGroup


        public int CompareTo(ReplyItem other)
        {
            return this.Name.CompareTo(other.Name);
        }
    }
}
