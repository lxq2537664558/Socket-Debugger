﻿//------------------------------------------------------------------------------
// <auto-generated>
//     此代码由工具生成。
//     运行时版本:4.0.30319.18444
//
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
// </auto-generated>
//------------------------------------------------------------------------------

namespace SocketClient {
    using System;
    
    
    /// <summary>
    ///   一个强类型的资源类，用于查找本地化的字符串等。
    /// </summary>
    // 此类是由 StronglyTypedResourceBuilder
    // 类通过类似于 ResGen 或 Visual Studio 的工具自动生成的。
    // 若要添加或移除成员，请编辑 .ResX 文件，然后重新运行 ResGen
    // (以 /str 作为命令选项)，或重新生成 VS 项目。
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Strings {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Strings() {
        }
        
        /// <summary>
        ///   返回此类使用的缓存的 ResourceManager 实例。
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("SocketClient.Strings", typeof(Strings).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   使用此强类型资源类，为所有资源查找
        ///   重写当前线程的 CurrentUICulture 属性。
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   查找类似 关于 的本地化字符串。
        /// </summary>
        internal static string About {
            get {
                return ResourceManager.GetString("About", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 添加 的本地化字符串。
        /// </summary>
        internal static string Add {
            get {
                return ResourceManager.GetString("Add", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 确定删除吗? 的本地化字符串。
        /// </summary>
        internal static string AreYouSureDelete {
            get {
                return ResourceManager.GetString("AreYouSureDelete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 确定删除项 &apos;{0}&apos; 吗? 的本地化字符串。
        /// </summary>
        internal static string AreYouSureDeleteItem {
            get {
                return ResourceManager.GetString("AreYouSureDeleteItem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 清除(&amp;L) 的本地化字符串。
        /// </summary>
        internal static string ClearLog {
            get {
                return ResourceManager.GetString("ClearLog", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 关闭 的本地化字符串。
        /// </summary>
        internal static string Close {
            get {
                return ResourceManager.GetString("Close", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 已关闭 的本地化字符串。
        /// </summary>
        internal static string Closed {
            get {
                return ResourceManager.GetString("Closed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 连接(&amp;C) 的本地化字符串。
        /// </summary>
        internal static string Connect {
            get {
                return ResourceManager.GetString("Connect", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 已连接: [{0}] {1} - {2} 的本地化字符串。
        /// </summary>
        internal static string ConnectdFromTo {
            get {
                return ResourceManager.GetString("ConnectdFromTo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 已连接 的本地化字符串。
        /// </summary>
        internal static string ConnectedState {
            get {
                return ResourceManager.GetString("ConnectedState", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 复制(&amp;Y) 的本地化字符串。
        /// </summary>
        internal static string Copy {
            get {
                return ResourceManager.GetString("Copy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 数据(Ascii) 的本地化字符串。
        /// </summary>
        internal static string DataAscii {
            get {
                return ResourceManager.GetString("DataAscii", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 数据(Hex) 的本地化字符串。
        /// </summary>
        internal static string DataHex {
            get {
                return ResourceManager.GetString("DataHex", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 方向 的本地化字符串。
        /// </summary>
        internal static string Direction {
            get {
                return ResourceManager.GetString("Direction", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 禁用 的本地化字符串。
        /// </summary>
        internal static string Disabled {
            get {
                return ResourceManager.GetString("Disabled", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 断开(&amp;D) 的本地化字符串。
        /// </summary>
        internal static string Disconnect {
            get {
                return ResourceManager.GetString("Disconnect", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 修改 的本地化字符串。
        /// </summary>
        internal static string Edit {
            get {
                return ResourceManager.GetString("Edit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 启用 的本地化字符串。
        /// </summary>
        internal static string Enabled {
            get {
                return ResourceManager.GetString("Enabled", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 从 的本地化字符串。
        /// </summary>
        internal static string From {
            get {
                return ResourceManager.GetString("From", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 无效的IP地址 的本地化字符串。
        /// </summary>
        internal static string InvalidIPAddress {
            get {
                return ResourceManager.GetString("InvalidIPAddress", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 无效的端口 的本地化字符串。
        /// </summary>
        internal static string InvalidPort {
            get {
                return ResourceManager.GetString("InvalidPort", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 &lt;--- 的本地化字符串。
        /// </summary>
        internal static string LeftArrow {
            get {
                return ResourceManager.GetString("LeftArrow", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 长度 的本地化字符串。
        /// </summary>
        internal static string Length {
            get {
                return ResourceManager.GetString("Length", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 35982772@qq.com 的本地化字符串。
        /// </summary>
        internal static string Mail {
            get {
                return ResourceManager.GetString("Mail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 未连接 的本地化字符串。
        /// </summary>
        internal static string NotConnectdState {
            get {
                return ResourceManager.GetString("NotConnectdState", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 打开 的本地化字符串。
        /// </summary>
        internal static string Open {
            get {
                return ResourceManager.GetString("Open", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 已打开 的本地化字符串。
        /// </summary>
        internal static string Opened {
            get {
                return ResourceManager.GetString("Opened", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 接收或回复数据长度不能为0 的本地化字符串。
        /// </summary>
        internal static string ReceivedReplyLengthZero {
            get {
                return ResourceManager.GetString("ReceivedReplyLengthZero", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 回复 的本地化字符串。
        /// </summary>
        internal static string Reply {
            get {
                return ResourceManager.GetString("Reply", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 回复: {0} 的本地化字符串。
        /// </summary>
        internal static string ReplyState {
            get {
                return ResourceManager.GetString("ReplyState", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 ---&gt; 的本地化字符串。
        /// </summary>
        internal static string RightArrow {
            get {
                return ResourceManager.GetString("RightArrow", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 请先选中一条记录 的本地化字符串。
        /// </summary>
        internal static string SelectListViewItemFirst {
            get {
                return ResourceManager.GetString("SelectListViewItemFirst", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 发送内容不能为空 的本地化字符串。
        /// </summary>
        internal static string SendCannotEmpty {
            get {
                return ResourceManager.GetString("SendCannotEmpty", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 数据不能为空 的本地化字符串。
        /// </summary>
        internal static string SendDataCannotEmpty {
            get {
                return ResourceManager.GetString("SendDataCannotEmpty", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 管理(&amp;M)... 的本地化字符串。
        /// </summary>
        internal static string SendDataManage {
            get {
                return ResourceManager.GetString("SendDataManage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 名称不能为空 的本地化字符串。
        /// </summary>
        internal static string SendDataNameCannotEmpty {
            get {
                return ResourceManager.GetString("SendDataNameCannotEmpty", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 保存(&amp;S)... 的本地化字符串。
        /// </summary>
        internal static string SendDataSave {
            get {
                return ResourceManager.GetString("SendDataSave", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 TCP 的本地化字符串。
        /// </summary>
        internal static string TCP {
            get {
                return ResourceManager.GetString("TCP", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 时间 的本地化字符串。
        /// </summary>
        internal static string Time {
            get {
                return ResourceManager.GetString("Time", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 到 的本地化字符串。
        /// </summary>
        internal static string To {
            get {
                return ResourceManager.GetString("To", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 转发 的本地化字符串。
        /// </summary>
        internal static string Transmit {
            get {
                return ResourceManager.GetString("Transmit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 转发: {0} 的本地化字符串。
        /// </summary>
        internal static string TransmitState {
            get {
                return ResourceManager.GetString("TransmitState", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 UDP 的本地化字符串。
        /// </summary>
        internal static string UDP {
            get {
                return ResourceManager.GetString("UDP", resourceCulture);
            }
        }
    }
}
